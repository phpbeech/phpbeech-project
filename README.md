[![N|Solid](https://image.flaticon.com/icons/png/128/294/294567.png)](https://nodesource.com/products/nsolid)
# PHP Beech framework
##### #Make it by yourself
#
#
## # Server Requirements
    PHP >= 5.3.0
#
## # Installing Beech
#
> Beech use Composer to manage its dependencies. So, before using Beech, make sure you have [Composer](https://getcomposer.org/) installed on your machine.
> Download the Beech installer using Composer.

    $ composer create-project bombkiml/phpbeech:dev-0.2-alpha "yourProjectName"
#
## # Development
> Want to contribute or join for Great Job!. You can contact to me via
E-mail nattapat.jquery@gmail.com or Facebook : [bombkiml](https://www.facebook.com/bombkiml)
#
## # License
> PHP Beech framework is open-sourced software licensed under the [MIT license.](https://opensource.org/licenses/MIT)
